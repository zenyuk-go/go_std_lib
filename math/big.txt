add two big.Int
==
i := new(big.Int)
r := i.Add(big.NewInt(2), big.NewInt(3))
println(i.String())  // 5
println(r.String())  // 5


