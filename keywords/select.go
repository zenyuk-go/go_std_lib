package main

import "time"

func main() {
	c1 := make(chan struct{})
	c2 := make(chan struct{})

	go notify(c2)

	select {
	case <-c1:
		println("c1 select")
	case <-c2:
		println("c2 select")
	}
}

func notify(c chan<- struct{}) {
	time.Sleep(time.Second)
	c <- struct{}{}
}
