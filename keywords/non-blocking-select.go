package main

import "time"

func main() {
	t := time.NewTicker(time.Second)
	println("start")

	for {
		select {
		case <-t.C:
			println("ticker ++++++++++++++")
		default:
		}
		time.Sleep(time.Millisecond * 300)
		println("default")
	}

	println("finish")
}
