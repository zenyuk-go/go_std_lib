package sort

import (
	"sort"
	"testing"
)

func TestSearch(t *testing.T) {
	// arrange
	s := []string{"bla", "e1", "mu"}

	// act
	// slice must be sorted first!
	sort.Strings(s)
	index := sort.SearchStrings(s, "e1")

	// assert
	if index != 1 {
		t.Error("search is broken")
	}
}

func TestSearchNotExisting(t *testing.T) {
	// arrange
	s := []string{"bla", "e1", "mu"}

	// act
	sort.Strings(s)
	index := sort.SearchStrings(s, "not_existing_item")

	// assert
	// for not existing key Search returns int equal to length
	if index < len(s) {
		t.Error("search is broken")
	}
}
