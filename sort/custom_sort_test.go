package sort

import (
	"sort"
	"testing"
)

type MyType struct {
	bla int
	mu  string
}

// create helper type - sort by
type ByBla []MyType

// implement 3 methods
func (m ByBla) Len() int {
	return len(m)
}
func (m ByBla) Less(i, j int) bool {
	return m[i].bla < m[j].bla
}
func (m ByBla) Swap(i, j int) {
	m[i], m[j] = m[j], m[i]
}

func TestCustomSort(t *testing.T) {
	// arrange
	s := []MyType{
		{3, "Nunu"},
		{2, "Xru"},
		{5, "Tutu"},
	}

	// act
	sort.Sort(ByBla(s))

	// assert
	if s[1].mu != "Nunu" {
		t.Error("custom sorting didn't work correctly")
	}
}
