package sort

import (
	"sort"
	"testing"
)

type MyType2 struct {
	bla int
	mu  string
}

func TestCustomComparatorSort(t *testing.T) {
	// arrange
	s := []MyType2{
		{3, "Nunu"},
		{2, "Xru"},
		{5, "Tutu"},
	}

	// act
	// sort with a custom comparator
	sort.SliceStable(s, func(i, j int) bool {
		return s[i].bla < s[j].bla
	})

	// assert
	if s[1].mu != "Nunu" {
		t.Error("custom sorting didn't work correctly")
	}
}
