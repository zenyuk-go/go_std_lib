package sort

import (
	"fmt"
	"sort"
	"testing"
)

func TestGuessingGame(t *testing.T) {
	var s string
	fmt.Printf("Pick an integer from 0 to 100.\n")
	answer := sort.Search(100, func(i int) bool {
		fmt.Printf("Is your number <= %d? ", i)
		fmt.Scanf("%s", &s)
		return s != "" && s[0] == 'y'
	})
	fmt.Printf("Your number is %d.\n", answer)
}

func TestIntSort(t *testing.T) {
	// arrange
	s := []int{5, 3, 4}

	// act
	sort.Ints(s)

	// assert
	if s[1] != 4 {
		t.Error("not sorted correctly")
	}
}

func TestStringSort(t *testing.T) {
	// arrange
	s := []string{"by", "abc", "be"}

	// act
	sort.Strings(s)

	// assert
	if s[1] != "be" {
		t.Error("not sorted correctly")
	}
}
