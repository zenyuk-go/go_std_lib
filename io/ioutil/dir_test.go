package ioutil

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func TestListDirs(t *testing.T) {
	fileInfos, err := ioutil.ReadDir("/home/u/temp")
	if err != nil {
		panic(err)
	}
	for _, f := range fileInfos {
		fmt.Printf("is dir? - %t, name = %s\n", f.IsDir(), f.Name() f.Sys())
	}

}
