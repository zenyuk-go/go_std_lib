package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

func main() {
	ch := make(chan string)
	for _, url := range os.Args[1:] {
		go SaveWebPageToFile(url, ch)
	}
	urlsCount := len(os.Args[1:])
	for i := 0; i < urlsCount; i++ {
		fmt.Printf("\ngot result: %s\n", <-ch)
	}
}

func SaveWebPageToFile(url string, ch chan<- string) {
	rsp, err := http.Get(url)
	if err != nil {
		ch <- fmt.Sprintf("error getting URL: %s, details: %s\n", url, err.Error())
		return
	}

	body, err := ioutil.ReadAll(rsp.Body)
	if err != nil {
		fmt.Printf("error reading http response: %s", err.Error())
		os.Exit(1)
	}
	fileName := urlToFileName(url)
	err = ioutil.WriteFile(fileName, body, 0644)
	if err != nil {
		fmt.Printf("error writing to file: %s, details: %s", fileName, err.Error())
		os.Exit(1)
	}

	rsp.Body.Close()
	ch <- fmt.Sprintf("done saving URL: %s", url)
}

func urlToFileName(url string) string {
	if strings.HasPrefix(url, "http://") {
		return strings.Replace(url, "http://", "", 1)
	}
	return url
}
