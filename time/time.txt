convert time and timestamp
==
t := time.Now() //2022-08-08 17:12:35.032221 +1000 AEST m=+0.000132251
fmt.Println(t)

//Get current time in count from 1970
fmt.Println(t.Unix()) //1659942755

//Get current time
fmt.Println(t.Format("2006-01-02 15:04:05"))  //2022-08-08 17:12:35

//Time to timestamp with Location
loc, _ := time.LoadLocation("Asia/Shanghai") // set the time zone
tt, _ := time.ParseInLocation("2006-01-02 15:04:05", "2018-07-11 15:07:51", loc)
fmt.Println(tt.Unix())								// 1531292871

//Timestamp to time
tm := time.Unix(1531293019, 0)			// 2018-07-11 17:10:19 +1000 AEST
fmt.Println(tm)

