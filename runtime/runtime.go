package main

import (
	"runtime"
)

func main() {
	for {
		// heavy calculations here

		// let schedule to interrupt this routine
		runtime.Gosched()
	}
}